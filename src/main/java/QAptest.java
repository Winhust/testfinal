import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QAptest {
    WebDriver driver;

    @Before
    public void Before() {
        System.out.println("Start");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/webdriver/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://au-webhook-adc1.onshopbase.com");
        driver.manage().window().maximize();
    }

    @After
    public void After() {
        System.out.println("Finish");
    }

    @Test
    public void Test() throws InterruptedException {
        String address;
        String discountcode;
        String discountvalue;
        String payment;
        String name;

        System.out.println("Test");
        //add to cart

        clickElement("(//button[@type='button'])[2]");
        inputToElement("//input[@id='search']", "Dây Kháng Lực Đa năng Tập Gym");
        Thread.sleep(10000);
        clickElement("(//a[@data-testid='productLink'])[1]");
        clickElement("(//button[@data-testid='addToCart'])[1]");
        Thread.sleep(5000);

        //Check out
        clickElement("//button[@name='checkout']");
        inputToElement("//input[@type='email']", "nguyenquyetthanghust@gmail.com");
        clickElement("(//span[@class='s-check'])[1]");
        inputToElement("//input[@name='first-name']", "Nguyen");
        address = "Nguyen ";
        inputToElement("//input[@name='last-name']", "Thang");
        address = address + "Thang";
        name = "Nguyen Thang";
        inputToElement("//input[@name='street-address']", "Ha Noi");
        address = address + "Ha Noi ";
        inputToElement("//input[@name='apartment-number']", "123");
        address = address + "123 ";
        inputToElement("//input[@name='city']", "Ha Noi");
        address = address + "Ha Noi ";
        inputToElement("//input[@name='zip-code']", "123456");
        address = address + "123456 Vietnam ";
        inputToElement("//input[@name='phone-number']", "123456789");
        address = address + "123456789";
        clickElement("(//span[@class='s-check'])[2]");
        inputToElement("//input[@placeholder='Enter your discount code here']", "testing");
        discountcode = "testing";
        clickElement("//button[@class='s-button field__input-btn']");
        discountvalue = msg("(//span[@class='order-summary__emphasis'])[2]");
        Thread.sleep(5000);
        clickElement("//button[@class='s-button step__continue-button']");
        Thread.sleep(5000);
        payment = msg("//span[@class='payment-due__price']");
        clickElement("//button[@class='s-button step__continue-button']");
        Thread.sleep(5000);

        driver.switchTo().frame(0);
        inputToElement("//input[@name='cardnumber' and @placeholder='Card number']", "4242 4242 4242 4242");
        driver.switchTo().parentFrame();

        inputToElement("//input[@placeholder='Cardholder name']", "Nguyen Van A");

        driver.switchTo().frame(1);
        inputToElement("//input[@name='exp-date']", "0923");
        driver.switchTo().parentFrame();

        driver.switchTo().frame(2);
        inputToElement("//input[@name='cvc']", "000");
        driver.switchTo().parentFrame();

        clickElement("//button[@class='s-button step__continue-button']");
        Thread.sleep(3000);
        clickElement("(//a[@class='app-mt16 upsell-only-blocks__next'])[1]");


        //verify order thank you page
        //verify shipping address
        if ((msg("(//address[@class='address'])[1]")).equals(address) == true) {
            System.out.println("Shipping address chinh xac ");
        } else {
            System.out.println("Shipping address sai");
        }
        //verify discount code
        if (msg("//span[@class='reduction-code__text']").equalsIgnoreCase(discountcode) == true) {
            System.out.println("Discount code chinh xac ");
        } else {
            System.out.println("Discount code sai");
        }
        //verify discount value
        if (msg("(//span[@class='order-summary__emphasis'])[2]").equals(discountvalue) == true) {
            System.out.println("Discount value chinh xac ");
        } else {
            System.out.println("Discount value sai");
        }
        //verify total
        if (msg("//span[@class='payment-due__price']").equals(payment) == true) {
            System.out.println("Total chinh xac ");
        } else {
            System.out.println("Total  sai");
        }


        //search + check order dashboard

        driver.get("https://accounts.shopbase.com/sign-in");
        driver.manage().window().maximize();

        inputToElement("//input[@id='email']", "shopbase2@beeketing.net");
        inputToElement("//input[@id='password']", "*esAS!z(:YeZ-5q");
        clickElement("//button[@type='submit']");
        clickElement("(//span[@class='ui-login_domain-label'])[136]");
        clickElement("(//span[@class='unite-ui-dashboard__aside--text'])[2]");
        inputToElement("(//input[@placeholder='Search orders'])[2]", "1067");

        if (msg("//span[@class='order-total-value type--bold money--gray']").equals(payment)==true) {
            System.out.println("total chinh xac");
        } else {
            System.out.println("total sai");
        }
        if (msg("//span[@class='order-customer-name money--gray']").equals(name)==true) {
            System.out.println("name chinh xac");
        } else {
            System.out.println("name sai");
        }


    }

    public WebElement getElement(String xpath) {
        return driver.findElement((By.xpath(xpath)));
    }

    public void clickElement(String xpath) {
        waitElement(xpath);
        getElement(xpath).click();
    }

    public String getElementText(String xpath) {
        waitElement(xpath);
        return getElement(xpath).getText();
    }

    public void inputToElement(String xpath, String value) {
        waitElement(xpath);
        WebElement e = getElement(xpath);
        e.clear();
        e.sendKeys(value);
    }

    public void waitElement(String xpath) {
        WebDriverWait driverWait = new WebDriverWait(driver, 30);
        driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
    }

    public String msg(String xpath) {
        waitElement(xpath);
        return getElementText(xpath);
    }
}


